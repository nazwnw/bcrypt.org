// --------------------------------------------------------------------------------------------------------------------

// npm
const yid = require('yid')
const LogFmtr = require('logfmtr')

// local
const log = require('./log.js')

// --------------------------------------------------------------------------------------------------------------------

function middleware(app) {

  app.use((req, res, next) => {
    // add a Request ID
    req._rid = yid()

    // create a RequestID and set it on the `req.log`
    req.log = log.withFields({ rid: req._rid })

    next()
  })

  app.use(LogFmtr.middleware)

  app.use((req, res, next) => {
    // set a `X-Made-By` header :)
    res.setHeader('X-Made-By', 'Andrew Chilton - https://chilts.org - @andychilton')

    // From: http://blog.netdna.com/opensource/bootstrapcdn/accept-encoding-its-vary-important/
    res.setHeader('Vary', 'Accept-Encoding')

    res.locals.title  = false

    // add the advert
    res.locals.ad = {
      title : 'Digital Ocean',
      url   : 'https://www.digitalocean.com/?refcode=c151de638f83',
      src   : 'https://s18.postimg.org/zbi91biah/digital-ocean-728x90.jpg',
      text1 : 'We recommend ',
      text2 : ' for hosting your sites. Free $10 credit when you sign up.',
    }

    next()
  })

}

// --------------------------------------------------------------------------------------------------------------------

module.exports = middleware

// --------------------------------------------------------------------------------------------------------------------
