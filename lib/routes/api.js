// --------------------------------------------------------------------------------------------------------------------

// npm
const bcrypt = require('bcrypt')

// local
const env = require('../env.js')

// --------------------------------------------------------------------------------------------------------------------

function routes(app) {

  app.post("/api/generate-hash.json", (req, res) => {
    const password = req.body.password || ''
    const cost = req.body.cost | 0

    if ( password.length === 0 ) {
      res.json({ ok : false, msg : 'Password must be provided.' })
      return
    }

    if ( cost < 4 || cost > 10 ) {
      res.json({ ok : false, msg : 'Cost must be between 4 and 10 (inclusive).' })
      return
    }

    // ToDo: generate the password.
    // { ok : true, msg : '', password : '', hash : '', cost : 10 }
    bcrypt.hash(password, cost, (err, hash) => {
      if (err) return next(err)
      res.json({
        ok       : true,
        msg      : 'Password hashed successfully.',
        password,
        hash,
        cost,
      })
    })
  })

  app.post("/api/check-password.json", (req, res) => {
    const password = req.body.password || ''
    const hash = req.body.hash || ''

    if ( password.length === 0 ) {
      res.json({ ok : false, msg : 'Password must be provided.' })
      return
    }

    if ( hash.length === 0 ) {
      res.json({ ok : false, msg : 'Hash must be provided.' })
      return
    }

    console.log('p=' + password)
    console.log('h=' + hash)

    bcrypt.compare(password, hash, (err, ok) => {
      if (err) return next(err)

      // if not the same
      if (!ok) {
        res.json({
          ok       : false,
          msg      : 'Correct password.',
        })
        return
      }

      // all correct
      res.json({
        ok       : true,
        msg      : 'Password checked successfully.',
        cost     : bcrypt.getRounds(hash),
        password,
        hash,
      })
    })
  })

}

// --------------------------------------------------------------------------------------------------------------------

module.exports = routes

// --------------------------------------------------------------------------------------------------------------------
